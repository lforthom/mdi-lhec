# MDI LHeC

## BDSIM

This project uses the BDSIM simulation tool and the accompanying pybdsim python tools. This can be accessed [here](http://www.pp.rhul.ac.uk/bdsim/manual/)

L.J. Nevay et al., BDSIM: An Accelerator Tracking Code with Particle-Matter Interactions, Computer Physics Communications 252 107200 (2020).
https://doi.org/10.1016/j.cpc.2020.107200

## The repository

This repository collects the BDSIM scritps used to study the Interaction Region (IR) for the LHeC project.

Link to Daniel's Google Drive from 2021 [here](https://drive.google.com/drive/folders/1W2H-tuY1Ab1UfIzDrjagIFt0New-n1Wo)

Link to Connor's github repository from 2020 [here](https://github.com/connorkm2/CERN-Summer-Internship).


_Single beam separation scheme study_ have the input and output files from Daniel's masther thesis studies featuring the single dipole optimisation.

_Combined beam separation scheme study_ have the input and output files from Daniel's masther thesis studies featuring the combined optimisation of dipoles, off-centered quadrupoles and half-quadrupoles.

_BDSIM examples_ have input files for **synchrotron radiation physics** simulations and **electro-magnetic interaction physics** from a beam on a target.

_LHeC Dan_ feature the latest input files from Daniel's master thesis.

_LHeC gmad_ feature Kevin's input files.
